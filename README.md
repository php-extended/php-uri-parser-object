# php-extended/php-uri-parser-object

A library that implements the php-extended/php-uri-parser-interface library.


![coverage](https://gitlab.com/php-extended/php-uri-parser-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-uri-parser-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-uri-parser-object ^8`


## Basic Usage

This library may be used the following way :

To parse an uri :

```php

use PhpExtended\Uuid\UuidParser;

$parser = new UriParser();
$uri = $parser->parse('https://packagist.org/path/to/repo');
// $uri instanceof UriInterface

```


## License

MIT (See [license file](LICENSE)).
