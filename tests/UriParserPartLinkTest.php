<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uri-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\Uri;
use PhpExtended\Uri\UriParserPartLink;
use PHPUnit\Framework\TestCase;

/**
 * UriParserPartLinkTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Uri\UriParserPartLink
 *
 * @internal
 *
 * @small
 */
class UriParserPartLinkTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UriParserPartLink
	 */
	protected UriParserPartLink $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testParsePartNoNext() : void
	{
		$this->assertEquals(new Uri(), $this->_object->parsePart(new Uri(), 0, '', ''));
	}
	
	public function testParsePartNext() : void
	{
		$this->_object = new UriParserPartLink(new UriParserPartLink());
		$this->assertEquals(new Uri(), $this->_object->parsePart(new Uri(), 0, '', ''));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UriParserPartLink();
	}
	
}
