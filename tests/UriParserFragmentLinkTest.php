<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uri-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\Uri;
use PhpExtended\Uri\UriParserFragmentLink;
use PHPUnit\Framework\TestCase;

/**
 * UriParserFragmentLinkTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Uri\UriParserFragmentLink
 *
 * @internal
 *
 * @small
 */
class UriParserFragmentLinkTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UriParserFragmentLink
	 */
	protected UriParserFragmentLink $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testParseFragment() : void
	{
		$uri = new Uri();
		$uri = $uri->withFragment('frag');
		$this->assertEquals($uri, $this->_object->parsePart(new Uri(), 0, '#frag', ''));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UriParserFragmentLink();
	}
	
}
