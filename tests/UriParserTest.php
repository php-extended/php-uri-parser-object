<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uri-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\Uri;
use PhpExtended\Parser\ParseException;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * UriParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Uri\UriParser
 *
 * @internal
 *
 * @small
 */
class UriParserTest extends TestCase
{
	
	/**
	 * The parser to test.
	 * 
	 * @var UriParser
	 */
	protected UriParser $_parser;
	
	public function testParseNull() : void
	{
		$this->assertEquals(new Uri(), $this->_parser->parse(null));
	}
	
	public function testParseFullUrl() : void
	{
		$expected = new Uri();
		$expected = $expected
			->withScheme('https')
			->withUserInfo('user', 'password')
			->withHost('packagist.org')
			->withPort(443)
			->withPath('/path/to/file')
			->withQuery('param=value')
			->withFragment('frag')
		;
		
		$this->assertEquals($expected, $this->_parser->parse('https://user:password@packagist.org:443/path/to/file?param=value#frag'));
	}
	
	public function testNoScheme() : void
	{
		$expected = new Uri();
		$expected = $expected
			->withUserInfo('user', 'password')
			->withHost('packagist.org')
			->withPort(443)
			->withPath('/path/to/file')
			->withQuery('param=value')
			->withFragment('frag')
		;
		
		$this->assertEquals($expected, $this->_parser->parse('user:password@packagist.org:443/path/to/file?param=value#frag'));
	}
	
	public function testCreateHostPortUrl() : void
	{
		$expected = new Uri();
		$expected = $expected
			->withScheme('https')
			->withUserInfo('toto', 'tata')
			->withHost('example.com')
			->withPort(8888)
		;
		
		$this->assertEquals($expected, $this->_parser->parse('//toto:tata@example.com:8888'));
	}
	
	public function testInvalidScheme() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('foobar://packagist.org');
	}
	
	public function testNoUserInfo() : void
	{
		$expected = new Uri();
		$expected = $expected
			->withScheme('https')
			->withHost('packagist.org')
			->withPort(443)
			->withPath('/path/to/file')
			->withQuery('param=value')
			->withFragment('frag')
		;
		
		$this->assertEquals($expected, $this->_parser->parse('https://packagist.org:443/path/to/file?param=value#frag'));
	}
	
	public function testArobaseInFrag() : void
	{
		$expected = new Uri();
		$expected = $expected
			->withScheme('https')
			->withHost('packagist.org')
			->withPort(443)
			->withPath('/path/to/file')
			->withQuery('param=value')
			->withFragment('frag@home')
		;
		
		$this->assertEquals($expected, $this->_parser->parse('https://packagist.org:443/path/to/file?param=value#frag@home'));
	}
	
	public function testInvalidPort() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('https://packagist.org:443000/path/to/file');
	}
	
	public function testInvalidHost() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('https://packagist...org:443/path/to/file');
	}
	
	public function testNoQuery() : void
	{
		$expected = new Uri();
		$expected = $expected
			->withScheme('https')
			->withHost('packagist.org')
			->withPort(443)
			->withPath('/path/to/file')
			->withFragment('frag')
		;
		
		$this->assertEquals($expected, $this->_parser->parse('https://packagist.org:443/path/to/file#frag'));
		$this->assertEquals('https://packagist.org:443/path/to/file#frag', $expected->__toString());
	}
	
	public function testSlashEndingPath() : void
	{
		$expected = new Uri();
		$expected = $expected
			->withScheme('https')
			->withHost('packagist.org')
			->withPath('/path/to/file/')
		;
		
		$this->assertEquals($expected, $this->_parser->parse('https://packagist.org/path/to/file/'));
		$this->assertEquals('https://packagist.org/path/to/file/', $expected->__toString());
	}
	
	public function testNoFragment() : void
	{
		$expected = new Uri();
		$expected = $expected
			->withScheme('https')
			->withHost('packagist.org')
			->withPort(443)
			->withPath('/path/to///file')
			->withQuery('param=value')
		;
		
		$this->assertEquals($expected, $this->_parser->parse('https://packagist.org:443/path///to/file?param=value'));
		$this->assertEquals('https://packagist.org:443/path/to/file?param=value', $expected->__toString());
	}
	
	public function testNoPort() : void
	{
		$expected = new Uri();
		$expected = $expected
			->withScheme('https')
			->withHost('packagist.org')
			->withPath('/path/to/file')
			->withQuery('param=value')
			->withFragment('frag@home')
		;
		
		$this->assertEquals($expected, $this->_parser->parse('https://packagist.org/path/to/file?param=value#frag@home'));
	}
	
	public function testDoubleScheme() : void
	{
		$expected = new Uri();
		$expected = $expected
			->withScheme('http')
			->withHost('www.legrandlarge.com')
		;
		
		$this->assertEquals($expected, $this->_parser->parse('http://http://www.legrandlarge.com'));
	}
	
	public function testNoMissingParts() : void
	{
		$expected = new Uri();
		$expected = $expected
			->withScheme('https')
			->withHost('cards.scryfall.io')
			->withPath('/border_crop/front/4/0/4033ffe6-3e81-440f-bde7-0cf267a54b36.jpg')
		;
		
		$this->assertEquals($expected, $this->_parser->parse('https://cards.scryfall.io/border_crop/front/4/0/4033ffe6-3e81-440f-bde7-0cf267a54b36.jpg'));
		$this->assertEquals('https://cards.scryfall.io/border_crop/front/4/0/4033ffe6-3e81-440f-bde7-0cf267a54b36.jpg', $expected->__toString());
	}
	
	public function testOnlyDomainAvailable() : void
	{
		$this->assertEquals('https://sbconcept.pagesperso-orange.fr/', $this->_parser->parse('sbconcept.pagesperso-orange.fr//')->__toString());
	}
	
	public function testDomainPageAvailable() : void
	{
		$this->assertEquals('https://tpemoselle.pagesperso-orange.fr/index.htm', $this->_parser->parse('tpemoselle.pagesperso-orange.fr//index.htm')->__toString());
	}
	
	public function testOnlyFragmentAvailable() : void
	{
		$this->assertEquals('#'.\rawurlencode('/link/to/anchor'), $this->_parser->parse('#/link/to/anchor')->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_parser = new UriParser();
	}
	
}
