<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uri-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\Uri;
use PhpExtended\Parser\ParseException;
use PhpExtended\Uri\UriParserSchemeLink;
use PHPUnit\Framework\TestCase;

/**
 * UriParserSchemeLinkTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Uri\UriParserSchemeLink
 *
 * @internal
 *
 * @small
 */
class UriParserSchemeLinkTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UriParserSchemeLink
	 */
	protected UriParserSchemeLink $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testNoScheme() : void
	{
		$this->assertEquals(new Uri(), $this->_object->parsePart(new Uri(), 0, 'hostname.com', ''));
	}
	
	public function testSameScheme() : void
	{
		$this->assertEquals(new Uri(), $this->_object->parsePart(new Uri(), 2, '//hostname.com', ''));
	}
	
	public function testScheme() : void
	{
		$uri = new Uri();
		$uri = $uri->withScheme('sftp');
		$this->assertEquals($uri, $this->_object->parsePart(new Uri(), 0, 'sftp://hostname.com', ''));
	}
	
	public function testDoubleScheme() : void
	{
		$uri = new Uri();
		$uri = $uri->withScheme('http');
		$this->assertEquals($uri, $this->_object->parsePart(new Uri(), 0, 'https://http://hostname.com', ''));
	}
	
	public function testFailedScheme() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parsePart(new Uri(), 0, 'zorglub://hostname.com', '');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UriParserSchemeLink();
	}
	
}
