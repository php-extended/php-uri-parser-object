<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uri-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\Uri;
use PhpExtended\Uri\UriParserUserInfoLink;
use PHPUnit\Framework\TestCase;

/**
 * UriParserUserInfoLinkTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Uri\UriParserUserInfoLink
 *
 * @internal
 *
 * @small
 */
class UriParserUserInfoLinkTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UriParserUserInfoLink
	 */
	protected UriParserUserInfoLink $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testUserInfoUser() : void
	{
		$uri = new Uri();
		$uri = $uri->withUserInfo('user', null);
		$this->assertEquals($uri, $this->_object->parsePart(new Uri(), 0, 'user@hostname.com', ''));
	}
	
	public function testUserInfoUserPasswd() : void
	{
		$uri = new Uri();
		$uri = $uri->withUserInfo('user', 'passwd');
		$this->assertEquals($uri, $this->_object->parsePart(new Uri(), 0, 'user:passwd@hostname.com', ''));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UriParserUserInfoLink();
	}
	
}
