<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uri-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\Uri;
use PhpExtended\Uri\UriParserQueryLink;
use PHPUnit\Framework\TestCase;

/**
 * UriParserQueryLinkTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Uri\UriParserQueryLink
 *
 * @internal
 *
 * @small
 */
class UriParserQueryLinkTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UriParserQueryLink
	 */
	protected UriParserQueryLink $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testParseQueryEmpty() : void
	{
		$uri = new Uri();
		$uri = $uri->withQuery('key=value');
		$this->assertEquals($uri, $this->_object->parsePart(new Uri(), 0, '?key=value', ''));
	}
	
	public function testParseQueryFragment() : void
	{
		$uri = new Uri();
		$uri = $uri->withQuery('key=value');
		$this->assertEquals($uri, $this->_object->parsePart(new Uri(), 0, '?key=value#fragment', ''));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UriParserQueryLink();
	}
	
}
