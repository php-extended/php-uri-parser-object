<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uri-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\Uri;
use PhpExtended\Parser\ParseException;
use PhpExtended\Uri\UriParserHostnameLink;
use PHPUnit\Framework\TestCase;

/**
 * UriParserHostnameLinkTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Uri\UriParserHostnameLink
 *
 * @internal
 *
 * @small
 */
class UriParserHostnameLinkTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UriParserHostnameLink
	 */
	protected UriParserHostnameLink $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testParseHostnameEmpty() : void
	{
		$uri = new Uri();
		$uri = $uri->withHost('hostname.com');
		$this->assertEquals($uri, $this->_object->parsePart(new Uri(), 0, 'hostname.com', ''));
	}
	
	public function testParseHostnamePath() : void
	{
		$uri = new Uri();
		$uri = $uri->withHost('hostname.com');
		$this->assertEquals($uri, $this->_object->parsePart(new Uri(), 0, 'hostname.com/path', ''));
	}
	
	public function testParseHostnameQuery() : void
	{
		$uri = new Uri();
		$uri = $uri->withHost('hostname.com');
		$this->assertEquals($uri, $this->_object->parsePart(new Uri(), 0, 'hostname.com?query=value', ''));
	}
	
	public function testParseHostnameFragment() : void
	{
		$uri = new Uri();
		$uri = $uri->withHost('hostname.com');
		$this->assertEquals($uri, $this->_object->parsePart(new Uri(), 0, 'hostname.com#fragment', ''));
	}
	
	public function testParseHostnamePortEmpty() : void
	{
		$uri = new Uri();
		$uri = $uri->withHost('hostname.com');
		$uri = $uri->withPort(3306);
		$this->assertEquals($uri, $this->_object->parsePart(new Uri(), 0, 'hostname.com:3306', ''));
	}
	
	public function testParseHostnamePortPath() : void
	{
		$uri = new Uri();
		$uri = $uri->withHost('hostname.com');
		$uri = $uri->withPort(3306);
		$this->assertEquals($uri, $this->_object->parsePart(new Uri(), 0, 'hostname.com:3306/path', ''));
	}
	
	public function testParseHostnamePortQuery() : void
	{
		$uri = new Uri();
		$uri = $uri->withHost('hostname.com');
		$uri = $uri->withPort(3306);
		$this->assertEquals($uri, $this->_object->parsePart(new Uri(), 0, 'hostname.com:3306?query=value', ''));
	}
	
	public function testParseHostnamePortFragment() : void
	{
		$uri = new Uri();
		$uri = $uri->withHost('hostname.com');
		$uri = $uri->withPort(3306);
		$this->assertEquals($uri, $this->_object->parsePart(new Uri(), 0, 'hostname.com:3306#fragment', ''));
	}
	
	public function testParseHostnamePortExcess() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parsePart(new Uri(), 0, 'hostname.com:567890', '');
	}
	
	public function testParseHostnameInvalid() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parsePart(new Uri(), 0, 'host@name.com', '');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UriParserHostnameLink();
	}
	
}
