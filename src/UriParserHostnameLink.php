<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uri-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Uri;

use InvalidArgumentException;
use PhpExtended\Parser\ParseException;
use Psr\Http\Message\UriInterface;

/**
 * UriParserHostnameLink class file.
 * 
 * This parser parses the hostname part of an uri.
 * 
 * @author Anastaszor
 */
class UriParserHostnameLink extends UriParserPartLink
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Uri\UriParserPartLink::parsePart()
	 */
	public function parsePart(UriInterface $uri, int $offset, string $remaining, string $fullinput) : UriInterface
	{
		if(!empty($remaining))
		{
			$index = $offset;
			$data = $remaining;
			
			$pathpos = \mb_strpos($data, '/');
			if(false === $pathpos)
			{
				$pathpos = null;
			}
			
			$quespos = \mb_strpos($data, '?');
			if(false === $quespos)
			{
				$quespos = null;
			}
			
			$hashpos = \mb_strpos($data, '#');
			if(false === $hashpos)
			{
				$hashpos = null;
			}
			
			$maxlength = $pathpos ?? $quespos ?? $hashpos ?? (int) \mb_strlen($data);
			
			$host = (string) \mb_substr($data, 0, $maxlength);
			$remaining = (string) \mb_substr($data, $maxlength);
			
			$colonpos = \mb_strrpos($host, ':');
			if(false !== $colonpos)
			{
				$port = (int) (string) \mb_substr($host, $colonpos + 1);
				$host = (string) \mb_substr($host, 0, $colonpos);
				
				try
				{
					$uri = $uri->withPort($port);
				}
				catch(InvalidArgumentException $e)
				{
					$index += (int) \mb_strlen($host);
					$message = 'Failed to parse port value';
					
					throw new ParseException(UriInterface::class, $fullinput, $index + 1, $message, -1, $e);
				}
			}
			
			try
			{
				$uri = $uri->withHost($host);
				$index += $maxlength;
			}
			catch(InvalidArgumentException $e)
			{
				$message = 'Failed to parse host value';
				
				throw new ParseException(UriInterface::class, $fullinput, $index + 1, $message, -1, $e);
			}
			
			$offset = $index;
		}
		
		return parent::parsePart($uri, $offset, $remaining, $fullinput);
	}
	
}
