<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uri-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Uri;

use InvalidArgumentException;
use PhpExtended\Parser\ParseException;
use Psr\Http\Message\UriInterface;

/**
 * UriParserPathLink class file.
 * 
 * This parser parses the path part of an uri.
 * 
 * @author Anastaszor
 */
class UriParserPathLink extends UriParserPartLink
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Uri\UriParserPartLink::parsePart()
	 */
	public function parsePart(UriInterface $uri, int $offset, string $remaining, string $fullinput) : UriInterface
	{
		if(!empty($remaining))
		{
			$index = $offset;
			$data = $remaining;
			
			$quespos = \mb_strpos($data, '?');
			if(false === $quespos)
			{
				$quespos = null;
			}
			
			$hashpos = \mb_strpos($data, '#');
			if(false === $hashpos)
			{
				$hashpos = null;
			}
			
			$maxlength = $quespos ?? $hashpos ?? (int) \mb_strlen($data);
			
			$path = (string) \mb_substr($data, 0, $maxlength);
			$remaining = (string) \mb_substr($data, $maxlength);
			
			try
			{
				$uri = $uri->withPath($path);
				$index += (int) \mb_strlen($path);
			}
			// @codeCoverageIgnoreStart
			catch(InvalidArgumentException $e)
			{
				$message = 'Failed to parse path value';
				
				throw new ParseException(UriInterface::class, $fullinput, $index + 1, $message, -1, $e);
			}
			// @codeCoverageIgnoreEnd
		}
		
		return parent::parsePart($uri, $offset, $remaining, $fullinput);
	}
	
}
