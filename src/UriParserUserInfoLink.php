<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uri-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Uri;

use Psr\Http\Message\UriInterface;

/**
 * UriParserUserInfoLink class file.
 * 
 * This parser parses the user info part of an uri.
 * 
 * @author Anastaszor
 */
class UriParserUserInfoLink extends UriParserPartLink
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Uri\UriParserPartLink::parsePart()
	 */
	public function parsePart(UriInterface $uri, int $offset, string $remaining, string $fullinput) : UriInterface
	{
		if(!empty($remaining))
		{
			$data = $remaining;
			$arobpos = \mb_strpos($data, '@');
			if(false !== $arobpos)
			{
				$pathpos = \mb_strpos($data, '/');
				if(false === $pathpos)
				{
					$pathpos = \mb_strlen($data);
				}
				
				$quespos = \mb_strpos($data, '?');
				if(false === $quespos)
				{
					$quespos = \mb_strlen($data);
				}
				
				$hashpos = \mb_strpos($data, '#');
				if(false === $hashpos)
				{
					$hashpos = \mb_strlen($data);
				}
				
				$minLength = \min((int) \mb_strlen($data), $pathpos, $quespos, $hashpos);
				
				if($arobpos <= $minLength)
				{
					$user = (string) \mb_substr($data, 0, $arobpos);
					$pswd = null;
					$remaining = (string) \mb_substr($data, $arobpos + 1);
					
					$colonpos = \mb_strpos($user, ':');
					if(false !== $colonpos)
					{
						$pswd = (string) \mb_substr($user, $colonpos + 1);
						$user = (string) \mb_substr($user, 0, $colonpos);
					}
					
					$uri = $uri->withUserInfo($user, $pswd);
					$offset += $arobpos + 1;
				}
			}
		}
		
		return parent::parsePart($uri, $offset, $remaining, $fullinput);
	}
	
}
