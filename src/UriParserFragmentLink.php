<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uri-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Uri;

use Psr\Http\Message\UriInterface;

/**
 * UriParserFragmentLink class file.
 * 
 * This parser parses the fragment part of an uri.
 * 
 * @author Anastaszor
 */
class UriParserFragmentLink extends UriParserPartLink
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Uri\UriParserPartLink::parsePart()
	 */
	public function parsePart(UriInterface $uri, int $offset, string $remaining, string $fullinput) : UriInterface
	{
		$hashpos = \mb_strrpos($remaining, '#');
		if(false !== $hashpos)
		{
			$fragment = (string) \mb_substr($remaining, $hashpos + 1);
			$uri = $uri->withFragment($fragment);
			$remaining = (string) \substr($remaining, 0, $hashpos);
		}
		
		return parent::parsePart($uri, \mb_strlen($fullinput), $remaining, $fullinput);
	}
	
}
