<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uri-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Uri;

use PhpExtended\Parser\ParseThrowable;
use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * UriParserPartLink class file.
 * 
 * This represents a generic link to be extended that is able to parse a
 * specific part of an uri.
 * 
 * @author Anastaszor
 */
class UriParserPartLink implements Stringable
{
	
	/**
	 * The next link to the chain.
	 * 
	 * @var ?UriParserPartLink
	 */
	protected ?UriParserPartLink $_next;
	
	/**
	 * Builds a new UriParserPartLink with its next link.
	 * 
	 * @param UriParserPartLink $next
	 */
	public function __construct(?UriParserPartLink $next = null)
	{
		$this->_next = $next;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Parse this part. This methods should be inherited by the specifics and
	 * links.
	 * 
	 * @param UriInterface $uri
	 * @param integer $offset
	 * @param string $remaining
	 * @param string $fullinput
	 * @return UriInterface
	 * @throws ParseThrowable if the parsing on this link or the next fails
	 */
	public function parsePart(UriInterface $uri, int $offset, string $remaining, string $fullinput) : UriInterface
	{
		if(null === $this->_next)
		{
			return $uri;
		}
		
		return $this->_next->parsePart($uri, $offset, $remaining, $fullinput);
	}
	
}
