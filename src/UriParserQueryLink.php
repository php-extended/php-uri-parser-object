<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uri-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Uri;

use InvalidArgumentException;
use PhpExtended\Parser\ParseException;
use Psr\Http\Message\UriInterface;

/**
 * UriParserQueryLink class file.
 * 
 * This parser parses the query part of an uri.
 * 
 * @author Anastaszor
 */
class UriParserQueryLink extends UriParserPartLink
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Uri\UriParserPartLink::parsePart()
	 */
	public function parsePart(UriInterface $uri, int $offset, string $remaining, string $fullinput) : UriInterface
	{
		if(!empty($remaining))
		{
			$quespos = \mb_strpos($remaining, '?');
			if(false !== $quespos)
			{
				$data = $remaining;
				$query = (string) \mb_substr($data, $quespos + 1);
				$remaining = '';
				
				$hashpos = \mb_strpos($data, '#', $quespos);
				if(false !== $hashpos)
				{
					$offset += $quespos;
					$query = (string) \mb_substr($data, $quespos + 1, $hashpos - $quespos - 1);
					$remaining = (string) \mb_substr($data, $hashpos);
				}
				
				try
				{
					$uri = $uri->withQuery($query);
					$offset += (int) \mb_strlen($query);
				}
				// @codeCoverageIgnoreStart
				catch(InvalidArgumentException $e)
				{
					$message = 'Failed to parse query value';
					
					throw new ParseException(UriInterface::class, $fullinput, $offset + 1, $message, -1, $e);
				}
				// @codeCoverageIgnoreEnd
			}
		}
		
		return parent::parsePart($uri, $offset, $remaining, $fullinput);
	}
	
}
