<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uri-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Uri;

use PhpExtended\HttpMessage\Uri;
use PhpExtended\Parser\AbstractParser;
use Psr\Http\Message\UriInterface;

/**
 * UriParser class file.
 * 
 * This class is a simple implementation of the UriParserInterface.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Parser\AbstractParser<UriInterface>
 */
class UriParser extends AbstractParser implements UriParserInterface
{
	
	/**
	 * The chain of part parsers to parse full urls.
	 * 
	 * @var UriParserPartLink
	 */
	protected UriParserPartLink $_chain;
	
	/**
	 * Builds a new UriParser with its inner part fragments organised within
	 * a chain of responsibility pattern.
	 */
	public function __construct()
	{
		$query = new UriParserQueryLink();
		$path = new UriParserPathLink($query);
		$hostname = new UriParserHostnameLink($path);
		$userinfo = new UriParserUserInfoLink($hostname);
		$fragment = new UriParserFragmentLink($userinfo);
		$this->_chain = new UriParserSchemeLink($fragment);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : UriInterface
	{
		return $this->_chain->parsePart(new Uri(), 0, (string) $data, (string) $data);
	}
	
}
