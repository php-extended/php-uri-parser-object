<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-uri-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Uri;

use InvalidArgumentException;
use PhpExtended\Parser\ParseException;
use Psr\Http\Message\UriInterface;

/**
 * UriParserSchemeLink class file.
 * 
 * This parser parses the scheme part of an uri.
 * 
 * @author Anastaszor
 */
class UriParserSchemeLink extends UriParserPartLink
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Uri\UriParserPartLink::parsePart()
	 */
	public function parsePart(UriInterface $uri, int $offset, string $remaining, string $fullinput) : UriInterface
	{
		// if the string begins with //
		if(\strlen($remaining) > 2 && '/' === $remaining[0] && '/' === $remaining[1])
		{
			return parent::parsePart($uri, 2, (string) \substr($remaining, 2), $fullinput);
		}
		
		do
		{
			$schpos = \mb_strpos($remaining, '://');
			if(false === $schpos)
			{
				break;
			}
			
			$scheme = (string) \mb_substr($remaining, 0, $schpos);
			$strlen = (int) \mb_strlen($scheme);
			if(0 < $strlen)
			{
				try
				{
					$uri = $uri->withScheme($scheme);
				}
				catch(InvalidArgumentException $e)
				{
					$message = 'Failed to parse scheme value';
					
					throw new ParseException(UriInterface::class, $fullinput, $offset + 1, $message, -1, $e);
				}
			}
			
			$offset += (int) \mb_strlen($scheme) + 3;
			$remaining = (string) \mb_substr($remaining, $schpos + 3);
		}
		while(\mb_strpos($remaining, '://') !== false);
		
		return parent::parsePart($uri, $offset, $remaining, $fullinput);
	}
	
}
